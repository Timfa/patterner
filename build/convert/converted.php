<?php
set_time_limit(30000);

$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = false;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
$mime = "";
$imageSize = new stdClass();

// Check if image file is a actual image or invalid image

if(isset($_POST["submit"])) 
{
    $isImage = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    
    $mime = $isImage["mime"];
    $imageSize->x = $isImage[0];
    $imageSize->y = $isImage[1];

    if($isImage !== false) 
    {
        $uploadOk = true;
    } 
    else 
    {
        echo "File is not an image.";
    }

    if(/*$imageFileType != "jpg" && */$imageFileType != "png"/* && $imageFileType != "jpeg"*/) 
    {
        echo "Sorry, only PNG files are allowed.";
        $uploadOk = false;
    }
}

if ($uploadOk)
{
    //var_dump($_FILES["fileToUpload"]);
    $image = imagecreatefrompng($_FILES["fileToUpload"]["tmp_name"]);
    
    $image = ManipulateImage($image);

    header('Content-type: text/json');
    header('Content-Disposition: attachment');
    echo json_encode($image);
}

function ManipulateImage($image)
{
    global $imageSize;
    $result = new stdClass();
    $result->offset = false;
    $result->cells = [];

    for ($y = 0; $y < $imageSize->y; $y++)
    {
        array_push($result->cells, []);
        for ($x = 0; $x < $imageSize->x; $x++)
        {
            $rgb = imagecolorat($image, $x, $y);
            $r = ($rgb >> 16) & 0xFF;
            $g = ($rgb >> 8) & 0xFF;
            $b = $rgb & 0xFF;

            array_push($result->cells[$y], ($r + $g + $b) / 3 > 128? 0 : 1);
        }
    }
    return $result;
}