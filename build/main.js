var WHITE = 0;
var BLACK = 1;
var WHITEwX = 2;
var BLACKwX = 3;

var TOGGLECOLOR = 0;
var TOGGLEX = 1;

var WARN = 10;
var ERROR = 20;
var LINETYPE_BLACK = 1;
var LINETYPE_WHITE = 0;
var LINETYPE_ALTERNATE = -1;

var grid = { cells: [], offset: true, looping: false };
var error = [];
var newLineType = LINETYPE_ALTERNATE;

function reset ()
{
    grid.offset = true;
    grid.cells = [];
    for (var y = 0; y < 30; y++)
    {
        grid.cells.push([]);
        for (var x = 0; x < 30; x++)
        {
            grid.cells[y].push((y + 1) % 2);
        }
    }

    redraw();
}

reset();

var cursorAction = TOGGLECOLOR;

function redraw ()
{
    if (document.getElementById("autoXBtn").checked)
        doAutoX();

    document.getElementById("currAction").style = document.getElementById("autoXBtn").checked ? "display: none;" : "";
    document.getElementById("xBtn").style = document.getElementById("autoXBtn").checked ? "display: none;" : "";
    document.getElementById("clearXBtn").style = document.getElementById("autoXBtn").checked ? "display: none;" : "";

    grid.looping = document.getElementById("vLoopBtn").checked;
    error = JSON.parse(JSON.stringify(grid.cells));

    checkError();

    let html = "";
    let sideBar = "";
    let topBar = "<tr>";

    for (var x = 0; x < grid.cells[0].length; x++)
    {
        topBar += "<td class=\"" + (x % 2 == 0 ? "white" : "colored") + "\">" + (grid.cells[0].length - (x)) + "</td>";
    }

    topBar += "<tr>";

    for (var y = 0; y < grid.cells.length; y++)
    {
        html += "<tr>";
        sideBar += "<tr>";
        sideBar += "<td class=\"" + (y % 2 == grid.offset ? "white" : "colored") + "\">" + (grid.cells.length - (y)) + "</td>";

        for (var x = 0; x < grid.cells[y].length; x++)
        {
            let warn = "";
            if (error[y][x] >= WARN)
                warn = " warning";
            if (error[y][x] >= ERROR)
                warn = " error";

            html += "<td id=\"td" + x + "," + y + "\" class=\"" + (grid.cells[y][x] % 2 == 1 ? "colored" : "white") + warn + "\">";
            if (grid.cells[y][x] >= 2)
                html += "X";
            html += "</td>";
        }
        html += "</tr>";
        sideBar += "</tr>";
    }

    document.getElementById("pattern").innerHTML = html;
    document.getElementById("leftBar").innerHTML = sideBar;
    document.getElementById("rightBar").innerHTML = sideBar;

    document.getElementById("topBar").innerHTML = topBar;
    document.getElementById("bottomBar").innerHTML = topBar;

    for (var y = 0; y < grid.cells.length; y++)
    {
        for (var x = 0; x < grid.cells[y].length; x++)
        {
            let Xp = x;
            let Yp = y;
            document.getElementById("td" + x + "," + y).addEventListener("click", function (e)
            {
                toggleCell(Xp, Yp);
                console.log(Xp + ", " + Yp);
            });
        }
    }
}

function vLoopBtnPress ()
{
    setTimeout(() =>
    {
        clearXBtnPress();
        doAutoX();
        redraw();
    }, 50);
}

function selectLineType ()
{
    let val = document.getElementById('linetype').value;

    switch (val)
    {
        case "alternate": newLineType = LINETYPE_ALTERNATE; break;
        case "white": newLineType = LINETYPE_WHITE; break;
        case "black": newLineType = LINETYPE_BLACK; break;
    }
}

function autoXBtnPress ()
{
    redraw();
    colorBtnPress();
}

function flipRowsBtnPress ()
{
    grid.offset = !grid.offset;
    redraw();
}

function doAutoX ()
{
    for (var y = 0; y < grid.cells.length; y++)
    {
        for (var x = 0; x < grid.cells[y].length; x++)
        {
            grid.cells[y][x] %= 2;
        }
    }

    for (var y = 0; y < grid.cells.length; y++)
    {
        for (var x = 0; x < grid.cells[y].length; x++)
        {
            let yp = y - 1;
            if (yp < 0)
                yp = grid.cells.length - 1;

            if (grid.looping || y - 1 >= 0)
            {
                if ((y % 2 == grid.offset) == grid.cells[y][x] % 2 && grid.cells[yp][x] < 2)
                {
                    if (grid.cells[y][x] % 2 == grid.cells[yp][x] % 2)
                        grid.cells[yp][x] += 2;
                }
            }
        }
    }
}

function checkError ()
{
    for (var y = 0; y < grid.cells.length; y++)
    {
        for (var x = 0; x < grid.cells[y].length; x++)
        {
            let yp = y - 1;
            if (yp < 0)
                yp = grid.cells.length - 1;

            if (!grid.looping && (y == 0 || y == grid.cells.length - 1))
            {
                if (grid.cells[y][x] % 2 == (y % 2 == grid.offset))
                    error[y][x] = WARN;
            }

            if (grid.looping || y - 1 >= 0)
            {
                if ((y % 2 == grid.offset) == grid.cells[y][x] % 2 && grid.cells[yp][x] < 2)
                    error[y][x] = WARN;
            }

            if (grid.cells[y][x] >= 2)
            {
                let bad = false;
                let yp2 = (y + 1) % grid.cells.length;
                let yp3 = (y + 2) % grid.cells.length;

                if ((y % 2 == grid.offset) == grid.cells[y][x] % 2)
                    bad = true;

                if (grid.looping || y + 1 < grid.length)
                {
                    if (grid.cells[yp2][x] >= 2)
                    {
                        bad = true;
                        error[yp2][x] = ERROR;
                    }
                }

                if (grid.looping || y + 1 < grid.length)
                {
                    if (grid.cells[yp2][x] % 2 != grid.cells[y][x] % 2)
                    {
                        bad = true;
                        error[yp2][x] = ERROR;
                    }
                }

                if (grid.looping || y + 2 < grid.length)
                {
                    if (grid.cells[yp3][x] % 2 != grid.cells[y][x] % 2)
                    {
                        bad = true;
                        error[yp3][x] = ERROR;
                    }
                }

                if (bad)
                    error[y][x] = ERROR;
            }
        }
    }
}

function invertBtnPress ()
{
    for (var y = 0; y < grid.cells.length; y++)
    {
        let yP = y;

        setTimeout(() =>
        {
            for (var x = 0; x < grid.cells[yP].length; x++)
            {
                toggleCell(x, yP, TOGGLECOLOR);
            }
        }, y);

        redraw();
    }

    flipRowsBtnPress();
}

function toggleCell (x, y, action)
{
    action = action || cursorAction;
    switch (grid.cells[y][x])
    {
        case WHITE:
            if (action == TOGGLECOLOR)
                grid.cells[y][x] = BLACK;
            if (action == TOGGLEX)
                grid.cells[y][x] = WHITEwX;
            break;

        case BLACK:
            if (action == TOGGLECOLOR)
                grid.cells[y][x] = WHITE;
            if (action == TOGGLEX)
                grid.cells[y][x] = BLACKwX;
            break;

        case WHITEwX:
            if (action == TOGGLECOLOR)
                grid.cells[y][x] = BLACKwX;
            if (action == TOGGLEX)
                grid.cells[y][x] = WHITE;
            break;

        case BLACKwX:
            if (action == TOGGLECOLOR)
                grid.cells[y][x] = WHITEwX;
            if (action == TOGGLEX)
                grid.cells[y][x] = BLACK;
            break;
    }

    redraw();
}

function allBlackBtnPress ()
{
    for (var y = 0; y < grid.cells.length; y++)
    {
        for (var x = 0; x < grid.cells[y].length; x++)
        {
            if (grid.cells[y][x] >= 2)
                grid.cells[y][x] = BLACKwX;
            else
                grid.cells[y][x] = BLACK;
        }
    }

    redraw();
}

function allWhiteBtnPress ()
{
    for (var y = 0; y < grid.cells.length; y++)
    {
        for (var x = 0; x < grid.cells[y].length; x++)
        {
            if (grid.cells[y][x] >= 2)
                grid.cells[y][x] = WHITEwX;
            else
                grid.cells[y][x] = WHITE;
        }
    }

    redraw();
}

function clearXBtnPress ()
{
    for (var y = 0; y < grid.cells.length; y++)
    {
        for (var x = 0; x < grid.cells[y].length; x++)
        {
            grid.cells[y][x] %= 2;
        }
    }

    redraw();
}

function colorBtnPress ()
{
    cursorAction = TOGGLECOLOR;
    document.getElementById("cursor").innerText = "Toggle Color";
}

function xBtnPress ()
{
    if (!document.getElementById("autoXBtn").checked)
    {
        cursorAction = TOGGLEX;
        document.getElementById("cursor").innerText = "Toggle X";
    }
}

async function loadBtnPress ()
{
    let file = await document.getElementById("loadBtn").files[0].text();

    console.log(file);
    grid = JSON.parse(file);
    document.getElementById("vLoopBtn").checked = grid.looping;
    redraw();
}

function shiftLeftBtnPress ()
{
    for (var y = 0; y < grid.cells.length; y++)
    {
        let row = grid.cells[y].shift();
        grid.cells[y].push(row);
    }

    redraw();
}

function shiftRightBtnPress ()
{
    for (var y = 0; y < grid.cells.length; y++)
    {
        let row = grid.cells[y].pop();
        grid.cells[y].unshift(row);
    }

    redraw();
}

function shiftUpBtnPress ()
{
    let row = grid.cells.shift();
    grid.cells.push(row);
    grid.offset = !grid.offset;

    redraw();
}

function shiftDownBtnPress ()
{
    let row = grid.cells.pop();
    grid.cells.unshift(row);
    grid.offset = !grid.offset;

    redraw();
}

function saveBtnPress ()
{
    download("pattern.json", JSON.stringify(grid));
}

function download (filename, text) 
{
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

function widthplusBtnPress ()
{
    for (var y = 0; y < grid.cells.length; y++)
    {
        let color = (y + 1) % 2;
        if (newLineType == LINETYPE_BLACK)
            color = BLACK;

        if (newLineType == LINETYPE_WHITE)
            color = WHITE;

        grid.cells[y].push(color);
    }

    redraw();
}

function widthminusBtnPress ()
{
    for (var y = 0; y < grid.cells.length; y++)
        grid.cells[y].splice(grid.cells[y].length - 1, 1);

    redraw();
}

function heightplusBtnPress ()
{
    let color = (grid.cells.length + 1) % 2;
    if (newLineType == LINETYPE_BLACK)
        color = BLACK;

    if (newLineType == LINETYPE_WHITE)
        color = WHITE;

    let row = [];
    for (var i = 0; i < grid.cells[0].length; i++)
        row.push(color);
    grid.cells.push(row);

    redraw();
}

function heightminusBtnPress ()
{
    grid.cells.splice(grid.cells.length - 1, 1);
    redraw();
}

redraw();